#!/bin/bash

# Run as root

# Set variables and remove exit


#############################

exit

HOSTNAME=hostname
PASSWORD=password
BASH_CONF_URL='https://gitlab.com/yen3k/configs/-/raw/master/default/.bashrc'
FILES_URL='https://gitlab.com/yen3k/configs/-/raw/master/default/files'

#############################

# pacman
pacman-key --init && \
  pacman-key --populate archlinuxarm && \
  pacman -Sy --noconfirm --needed && \
  pacman -S vim bash-completion git ntp tmux man base-devel wget unzip dnsutils --noconfirm --needed && \
  # host \
  echo $HOSTNAME > /etc/hostname && \
  echo "127.0.0.1 localhost ${HOSTNAME} ${HOSTNAME}.localdomain" > /etc/hosts && \
  echo "::1 localhost ${HOSTNAME} ${HOSTNAME}.localdomain" >> /etc/hosts && \
  # locale \
  echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
  echo "en_US ISO-8859-1" >> /etc/locale.gen && \
  echo "da_DK.UTF-8 UTF-8" >> /etc/locale.gen && \
  echo "da_DK ISO-8859-1" >> /etc/locale.gen && \
  locale-gen && \
  echo LANG=en_US.UTF-8 > /etc/locale.conf && \
  # time \
  rm -f /etc/localtime && \
  ln -s /usr/share/zoneinfo/Europe/Copenhagen /etc/localtime && \
  systemctl enable ntpd.service && \
  systemctl start ntpd.service && \
  systemctl enable systemd-timesyncd.service && \
  systemctl start systemd-timesyncd.service && \
  ntpdate dk.pool.ntp.org && \
  systemctl restart systemd-timesyncd.service && \
  timedatectl set-ntp true && \
  # users \
  mkdir -p /root/.ssh && \
  chmod -R 700 /root/.ssh/ && \
  wget ${FILES_URL}/yen_rsa.pub -O /root/.ssh/authorized_keys && \
  chown -R root:root /root/.ssh/ && \
  chmod 600 -R /root/.ssh/authorized_keys && \
  echo -e "${PASSWORD}\n${PASSWORD}" | passwd root && \
  echo "PasswordAuthentication yes" > /etc/ssh/sshd_config && \
  echo "PermitRootLogin yes" >> /etc/ssh/sshd_config && \
  echo "PubkeyAuthentication yes" >> /etc/ssh/sshd_config && \
  echo "PubkeyAcceptedKeyTypes=+ssh-rsa" >> /etc/ssh/sshd_config && \
  echo "ChallengeResponseAuthentication no" >> /etc/ssh/sshd_config && \
  echo "UsePam yes" >> /etc/ssh/sshd_config && \
  echo "Subsystem sftp  /usr/lib/ssh/sftp-server" >> /etc/ssh/sshd_config && \
  systemctl restart sshd.service && \
  # conf \
  wget $BASH_CONF_URL -P /root && \
  echo -e "if [ -f ~/.bashrc ]; then\n        . ~/.bashrc\nfi" > /root/.profile

  # Remove alarm user manually
